import React from 'react';
import ReactMapGL from 'react-map-gl';
import {Component} from 'react';

export class Map extends Component {
    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = {
            viewport: {
              width: 400,
              height: 400,
              latitude: 37.7577,
              longitude: -122.4376,
              zoom: 8
            }
          };
      }
  
  
  render() {
    return (
    <div>
    <p>Mapa</p>
        <ReactMapGL
        mapboxApiAccessToken = {process.env.MAP_KEY}
        {...this.state.viewport}
        onViewportChange={(viewport) => this.setState({viewport})}
        />
    </div>
    );
  }
}


export default Map;
