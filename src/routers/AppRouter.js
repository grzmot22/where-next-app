import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import WhatNextDashboardPage from '../components/WhatNextDashboardPage';
import MapPage from '../components/MapPage';
import NotFoundPage from './../components/NotFoundPage';
import HelpPage from './../components/HelpPage';
import WebSocket from './../components/WebSockets';
import Header from './../components/Header';

const AppRouter = () => (
    <BrowserRouter>
        <div>
        <Header />
        <Switch>
        <Route path="/" component={WhatNextDashboardPage} exact={true}/>
        <Route path="/map" component={MapPage}/>
        <Route path="/help" component={HelpPage}/>
        <Route path="/webtest" component={WebSocket}/>
        <Route  component={NotFoundPage} />
        </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;