import React from 'react';
import {NavLink} from 'react-router-dom';

const Header = () => (
    <header>
    <h1>Whats Next?</h1>
    <NavLink to="/" activeClassName="is-active" exact={true}>Dashboard</NavLink>
    <NavLink to="/help" activeClassName="is-active">Help</NavLink>
    <NavLink to="/webtest" activeClassName="is-active">Test</NavLink>
    </header>
);

export default Header;