import React from 'react';
import { Link } from 'react-router-dom';

const WhatNextDashboardPage = () => (
    <div>
        <Link to="/map"><button>What Next</button></Link>
    </div>

);

export default WhatNextDashboardPage;