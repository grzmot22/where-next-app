const path = require('path');
const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');

module.exports = {
    entry: './src/app.js',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }, {
            test:/\.s?css$/,
            use: [
                'style-loader',
                'css-loader',
                'sass-loader'
            ]
        }]
    },
    mode: 'development',
    devtool: 'cheap-module-eval-source-map',
    plugins: [
        new Dotenv({
            path: './map-api-token.env',
            safe: false 
        })
      ],
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        historyApiFallback: true
    }
};